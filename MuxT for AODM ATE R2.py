#########################
#Modified by BEN
#TAKE SCREENSHOTS MANUALLY
#########################
import visa
import time
import xlsxwriter
import os
import shutil

rm = visa.ResourceManager()
E5071B = rm.open_resource('TCPIP0::10.0.10.177::inst0::INSTR')
#E5071B = rm.open_resource('GPIB1::17::INSTR')


serial_number = raw_input("Unit Serial Number:")
tester = raw_input("Tested by:")
#file_name = raw_input("File name:")
raw_input("Press Enter to Begin") 
##f = open(serial_number+'.txt','w')

workbook = xlsxwriter.Workbook(serial_number+'.xlsx')
worksheet = workbook.add_worksheet()

worksheet.write(0, 0, 'S/N:')
worksheet.write(1, 0, 'Tested by:')
worksheet.write(0, 1, str(serial_number))
worksheet.write(1, 1, str(tester))
delay = 0.75


def Write_to_Worksheet(column_name, array, column):

    worksheet.write(2, column, column_name)
    row = 3
    i = 0
    
    while i < len(temp_values):
        if ((i % 2) == 0):
            worksheet.write(row, column, str(temp_values[i]))
            row = row + 1
        i = i + 1
    return
    

#Write Frequency to Column A
############################################

E5071B.write(':DISPlay:WINDow1:ACTivate')
time.sleep(delay)
chan1_freq = E5071B.query_ascii_values(':SENSe1:FREQuency:DATA?')

worksheet.write(2, 0, 'Frequency')
row = 3
column = 0
i = 0

while i < len(chan1_freq):
    worksheet.write(row, column, str(chan1_freq[i]))
    row = row + 1
    i = i + 1

#Write S11 to Column B
############################################

E5071B.write(':DISPlay:WINDow1:ACTivate')
time.sleep(delay)
temp_values = E5071B.query_ascii_values(':CALCulate1:TRACe1:DATA:FDATa?')
Write_to_Worksheet('S11', temp_values, 1)

#Write S22 to Column B
############################################

E5071B.write(':DISPlay:WINDow1:ACTivate')
time.sleep(delay)
temp_values = E5071B.query_ascii_values(':CALCulate1:TRACe2:DATA:FDATa?')
Write_to_Worksheet('S22', temp_values, 2)

#Write S33 to Column C
############################################

E5071B.write(':DISPlay:WINDow1:ACTivate')
time.sleep(delay)
temp_values = E5071B.query_ascii_values(':CALCulate1:TRACe3:DATA:FDATa?')
Write_to_Worksheet('S33', temp_values, 3)

#Write S44 to Column D
############################################

E5071B.write(':DISPlay:WINDow1:ACTivate')
time.sleep(delay)
temp_values = E5071B.query_ascii_values(':CALCulate1:TRACe3:DATA:FDATa?')
Write_to_Worksheet('S44', temp_values, 4)

#Write S21 to Column E
############################################
E5071B.write(':DISPlay:WINDow2:ACTivate')
time.sleep(delay)
temp_values = E5071B.query_ascii_values(':CALCulate2:TRACe2:DATA:FDATa?')
Write_to_Worksheet('S21', temp_values, 5)

#Write S43 to Column F
############################################
E5071B.write(':DISPlay:WINDow2:ACTivate')
time.sleep(delay)
temp_values = E5071B.query_ascii_values(':CALCulate2:TRACe1:DATA:FDATa?')
Write_to_Worksheet('S43', temp_values, 6)
    
#Write S23 to Column G
############################################
E5071B.write(':DISPlay:WINDow3:ACTivate')
time.sleep(delay)
temp_values = E5071B.query_ascii_values(':CALCulate3:TRACe1:DATA:FDATa?')
Write_to_Worksheet('S23', temp_values, 7)

#Write S14 to Column H
############################################
E5071B.write(':DISPlay:WINDow3:ACTivate')
time.sleep(delay)
temp_values = E5071B.query_ascii_values(':CALCulate3:TRACe2:DATA:FDATa?')
Write_to_Worksheet('S14', temp_values, 8)

#################   

filed = "P:\\"+ serial_number + ".S4P" 
##Line to save S4P files
E5071B.write('MMEM:STOR:SNP:TYPE:S4P 1,2,3,4')
E5071B.write('MMEM:STOR:SNP "%s"' %filed)
time.sleep(1)

## Move file
path = os.path.dirname(__file__)
print str(path)

shutil.move(filed, path)

#setting the format to save a screenshot

E5071B.values_format.is_binary = True
E5071B.values_format.datatype = 'B'
E5071B.values_format.is_big_endian = False
E5071B.values_format.container = bytearray

#saving a screenshot on the equipment
##E5071B.write(':MMEMory:STORe:IMAGe "%s"' % ('D:Image01.png'))

#fetching the image data in an array
##fetch_image_data = block = E5071B.query_values(':MMEMory:TRANsfer? "%s"' % ('D:Image01.png'))

#creating a file with the product number to write the image data to it
##save_dir = open(str(serial_number)+".PNG", 'wb')
##save_dir.write(fetch_image_data)
##save_dir.close()

#deleting the image on the equipment
##E5071B.write(':MMEMory:DELete "%s"' % ('D:Image01.png'))
##E5071B.write("*CLS")

workbook.close()
##f.close()
E5071B.close()
rm.close()

##

