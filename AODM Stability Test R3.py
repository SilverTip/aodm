import sys
import pyvisa as visa
import time
import csv
import datetime
import socket
import xlsxwriter
import serial

serialPort = serial.Serial(port = "COM3", baudrate=115200,
                           bytesize=8, timeout=2, stopbits=serial.STOPBITS_ONE)


#TESTING PARAMETERS
######################################################
GATE_TIME = 5.0 #Set gate time for frequency counter
DELAY = 60 - GATE_TIME #Polling period
Err_threshold = 0.3 #0.3ppb
Err_threshold2 = 5 #5 ppb

#FUNCTIONS
######################################################
def timestamp():
    now = datetime.datetime.now()
    now = now.strftime("%d %b %Y %H:%M:%S")
    return now

def Get_Temp(): #Used to communicate with temperature probe on prod rack #1
    # Connect the socket to the port where the server is listening
    server_address = ('10.0.10.117', 2000)
    message = '*SRHC\r'
    message_byte = str.encode(message)
    try:
        #print 'connecting to %s port %s' % server_address
        #sock.close()
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(server_address)       
        # Send data

        #print 'sending "%s"' % message
        sock.sendall(message_byte)

        # Look for the response
        amount_received = 0
        amount_expected = len(message)
        
        while amount_received < amount_expected:
            data = sock.recv(16)
            amount_received += len(data)
        
    finally:
        sock.close()
        T_Chamber = data.strip()
        T_Chamber.lstrip()
        T_Chamberstr = T_Chamber.decode()
        
    return T_Chamberstr

#SCRIPT START
######################################################

#Workbook parameters
SERIAL_NUMBER = input("Device Serial Number: ")
TESTER = input("Tester Name: ")
kp = input("Kp: ")
ki = input("Ki: ")
kd = input("Kd: ")

rm = visa.ResourceManager()

#Connect to temperature sensor
print ("Temperature sensor connected")

#Connect to Multimeter
cur = rm.open_resource('GPIB0::23::INSTR')
cur.timeout = 600000
print ("Digital Multimeter connected")

#Connect to Frequency Counter
v53220A = rm.open_resource('TCPIP0::10.0.10.158::inst0::INSTR')
v53220A.timeout = 60000
print ("Frequency Counter connected")

#Create Workbook
TIMEFORMAT  = "%Y%m%d-%H%M" 
DATE        =  time.strftime(TIMEFORMAT, time.localtime(time.time()))
TITLE       = "AODM Stability Test "
workbook = xlsxwriter.Workbook(SERIAL_NUMBER + '_' + TITLE + '(' + DATE + ').xlsx')
worksheet = workbook.add_worksheet()
worksheet2 = workbook.add_worksheet()

#Write Workbook Headers
worksheet.write(0, 0, 'S/N:')
worksheet.write(1, 0, 'Tested by:')
worksheet.write(0, 1, str(SERIAL_NUMBER))
worksheet.write(1, 1, str(TESTER))
worksheet.write(2, 0, 'Time')
worksheet.write(2, 1, 'AODM Frequency (Hz)')
worksheet.write(2, 2, 'Freq Offset (ppb)')
worksheet.write(2, 3, 'Current (A)')
worksheet.write(2, 4, 'Temperature (C)')
worksheet.write(2, 5, 'PID values')
worksheet.write(3, 5, kp)
worksheet.write(4, 5, ki)
worksheet.write(5, 5, kd)
worksheet2.write(0, 0, 'DEBUG LOG')


dummy = input("Press Enter to Begin Test\n")

print('\n Press "CTRL + C" to stop test\n')

    
print('Time : Frequency(Hz) : Offset(ppb) : Current(A) : Temperature(C)')

row = 3
row2 = 3

prev_time = 0
    
while(1):

    try:
        timer = time.time()

        #Poll Serial Port
        if(serialPort.in_waiting > 0):
            serialString = serialPort.readline()
            debug_string = serialString.decode()
            debug_string = debug_string.rstrip("\n")
            current_time = timestamp()
            worksheet2.write(row2, 0, current_time)
            worksheet2.write(row2, 1, debug_string)
            print('DEBUG:  ',current_time, debug_string)
            row2 = row2 + 1

        #Read monitoring equipment every DELAY seconds
        if((timer - prev_time) > DELAY):

            prev_time = timer

            #Fetch Time
            current_time = timestamp()

            #Fetch Temperature Value
            temperature = Get_Temp()

            #Fetch DC Current Value
            temp_curr = cur.query_ascii_values('current')
            current = temp_curr[0]
            current = round(current, 3)
            
            #Fetch Channel 1 Frequency
            v53220A.write(':CONFigure:SCALar:VOLTage:FREQuency (%s)' % ('@1'))
            v53220A.write(':SENSe:FREQuency:GATE:TIME %G S' % (GATE_TIME))
            temp_values = v53220A.query_ascii_values(':READ?')
            freq1 = temp_values[0]

            #Get frequency offset from 10MHz
            scale1 = (freq1 - 10000000) * 100
            s1 = round(scale1, 3)
            
            #Write to Spreadsheet
            ##workbook.open()

            ##worksheet.write(row, column, column_name)
            worksheet.write(row, 0, current_time)
            worksheet.write(row, 1, freq1)
            worksheet.write(row, 2, s1)
            worksheet.write(row, 3, current)
            worksheet.write(row, 4, temperature)

            worksheet.write(row, 6, Err_threshold)
            worksheet.write(row, 7, (Err_threshold * -1))
            worksheet.write(row, 8, Err_threshold2)
            worksheet.write(row, 9, (Err_threshold2 * -1))
            
            row = row + 1
            ##workbook.close()
            

            #Print Values to Console
            print('MONITOR:', current_time, freq1, s1, current, temperature)


        #time.sleep(DELAY)


    except KeyboardInterrupt:
        error = "KeyboardInterrupt stopping the program..."
        print (error)
        worksheet.write(row, 0, error)
        workbook.close()
        break
